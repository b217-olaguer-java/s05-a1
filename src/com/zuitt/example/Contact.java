package com.zuitt.example;

public class Contact {
    private String name;
    private String contactNumber;
    private String address;

    public Contact(){

    }

    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }


    // Getters
    public String getName(){ return this.name; }
    public String getContactNumber(){ return this.contactNumber;}
    public String getAddress(){return this.address;}

    // Setters of Class Properties
    public void setName(String name){ this.name = name; }
    public void setContactNumber(String contactNumber){ this.contactNumber = contactNumber; }
    public void setAddress(String address){ this.address = address; }


    //methods
    public void registerNumber(){
        System.out.println(getName() + " has the following registered number:");
        System.out.println(getContactNumber());
    }

    public void speakAddress(){
        System.out.println(getName() + " has the following registered address:");
        System.out.println("my home in " + getAddress());
    }
};
