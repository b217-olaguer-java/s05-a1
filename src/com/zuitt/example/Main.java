package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;


public class Main {
    public static void main(String[] args) {

        System.out.println("\n");
        System.out.println("Activity");

        Phonebook book = new Phonebook();
        Contact contactOne = new Contact();
        Contact contactTwo = new Contact();

        contactOne.setName("Osama Bin laden");
        contactOne.setContactNumber("+63900000000");
        contactOne.setAddress("Jolo, Sulu, Philippines");

        contactTwo.setName("Ayman Mohammed Rabie al-Zawahiri");
        contactTwo.setContactNumber("+639111111111");
        contactTwo.setAddress("Cairo Egypt");

        book.setContacts(contactOne);
        book.setContacts(contactTwo);

        book.getContacts().forEach(Contact -> {
            System.out.println("-----------------");
            System.out.println(Contact.getName());
            System.out.println("-----------------");
            Contact.registerNumber();
            Contact.speakAddress();

            });


        };


    };

